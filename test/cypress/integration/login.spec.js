'use strict'

describe('Pruebas de login', () => {
  it('Debe cargar el login', () => {
    cy.visit('/login')
  })

  it('Debe registrar un usuario', () => {
    cy.visit('/login')
    cy.contains('Crear una cuenta').click()
    cy.get('#name').type('Usuario de prueba')
    cy.get('#title').type('Compañía de prueba')
    cy.get('#email2').type('test@test.com')
    cy.get('#password2').type('test1234')
    cy.contains('.button', 'Registrarse').click()
    cy.wait(3000)
    cy.get('.error-msg').should('not.exist')
  })

  it('Debe loguear un usuario', () => {
    cy.visit('/login')
    cy.get('#email1').type('test@test.com')
    cy.get('#password1').type('test1234')
    cy.contains('.button', 'Ingresar').click()
  })
})